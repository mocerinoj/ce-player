var player = $("#ce-player"),
    video = player.find(".video"),
    presentation = player.find(".presentation"),
    presentationImg = presentation.find("img"),
    presentationSpecial = presentation.find("div"),
    slides = player.find(".slides ul"),
    info = player.find(".info")
    presentationTitle = info.find(".details strong"),
    presentationSpeaker = info.find(".details span"),
    presentationDesc = info.find(".details p"),
    resources = info.find(".resources"),
    playlist = player.data("playlist"),
    slidePrefix = "slide_",
    slideThumbPrefix = "tSlide_",
    speakers = [],
    playerInstance = jwplayer("videoPlayer"),
    pauseTimes = [],
    disableScreen = $(".disable-screen");

$(function(){
  // load playlist
  $.when( loadPlaylist() ).done(function(){
    // setup slide nav
    slides.on("click","li",function(){
      // change out presentation slide
      changeSlide($(this).data("index"));
    });

    // setup form submits
    presentation.on("submit","form",function(e){
      e.preventDefault;
      var thisSlide = slides.find("[data-index='"+presentation.data("index")+"']"),
          quiz = thisSlide.data("quizdata"),
          $this = $(this),
          validation = $this.find(".validation");
      // post to server
      $.get("/post.aspx",{
        "userResponse": $this.find("input").val(),
        "questionID": quiz.questionid, // The questions in each activity are numbered 1-n.
        "theID": app_userID, // user id
        "courseID" : app_courseID // the courseID on the site.
      },function(resp){
        // returns non-standard xml so we need to do some regex
        var re = /\<row responseCount=\"([0-9]+)\" userResponse=\"([0-9]+)\"\/\>/gi,
            answer,
            answers = [],
            totalAnswers = 0;
        while ((answer = re.exec(resp)) !== null) {
          // answer[1] = amount answered
          // answer[2] = question (1-n)
          answers.splice(answer[2], 0, answer[1]);
        }
        totalAnswers = answers.reduce(function(a, b){return parseInt(a)+parseInt(b);});
        // mark correct answer
        $this.addClass("answered");
        $this.find("input[value='"+(parseInt(quiz.answerIndex)+parseInt(1))+"']").parent("label").addClass("correct");
        // add answer precentages next to
        $.each(answers, function(i, item){
          $this.find("input[value='"+(i+1)+"']").replaceWith($('<span/>',{
            "class": "answer",
            "text": Math.round((item/totalAnswers)*100)+"%"
          }));
        });

        // rename submit button, add next button??
        $this.find("button").text("Next").on("click",function(){
          changeSlide(parseInt(presentation.data("index"))+parseInt(1));
          return false;
        });

        /*// grade answer
        if($this.find("input:checked").val() != parseInt(quiz.answerIndex)+parseInt(1)){
          // show incorrect
          validation.show();
        }else{
          // erase form, show success
          presentationSpecial.html('<div class="alert alert-success">Yay!... now show averages</div>');
        }*/
      });
      return false;
    });
  });
});

// player functions
function loadPlaylist(){
  $.get(playlist, function(data){
    playerInit($.xml2json(data));
  }, "xml");
}

function playerInit(xml){
  var timeline = xml.level_1.myTimesArray.split(",")
      slideDir = slideDirThumb = xml.level_1.slidePath;

  // load thumbs
  var thumbs = [];
  for(i=1; i <= timeline.length; i++){
    thumbs.push($("<li/>",{
      "data-index": i,
      "data-isvideo": false,
      "data-isquiz": false,
      "data-time": timeline[i-1]
    }).append($("<img/>", {
      "src": xml.level_1.thumbPath + slideThumbPrefix + pad(i) + ".jpg"
    })));
  }
  // setup slide thumbs as a slideshow
  slides.html(thumbs).cycle({
    "slides": "li",
    "fx": "carousel",
    "paused": true,
    "prev": "#prev",
    "next": "#next",
    "allowWrap": false,
    "carouselVisible": 8,
    "carouselFluid": true,
    "swipe": true,
    "swipeFx": "scrollHorz"
    ,"log": false
  });
  // FIX THIS - bug after playing and pausing video slide, slideshow doesnt work?

  // loop through video and quiz slides to add data to thumb slides
  var items = [];
  if(xml.videoSlides.videoSlide.length > 0){
    items = xml.videoSlides.videoSlide;
  }else if(xml.videoSlides.videoSlide.slideNumber){
    items[0] = xml.videoSlides.videoSlide;
  }
  $.each(items, function(i, slide){
    slides.find("[data-index='"+slide.slideNumber+"']").data({
      "isvideo": true,
      "videodata": slide
    });
  });
  var items = [];
  if(xml.quizSlides.quizSlide.length > 0){
    items = xml.quizSlides.quizSlide;
  }else if(xml.quizSlides.quizSlide.slideNumber){
    items[0] = xml.quizSlides.quizSlide;
  }
  // FIX THIS - could be slideNumber="7" or slideNumber="07"
  $.each(items, function(i, slide){
    slide.questionid = i+1;
    slides.find("[data-index='"+slide.slideNumber+"']").data({
      "isquiz": true,
      "quizdata": slide
    });
    pauseTimes.push(timeline[parseInt(slide.slideNumber)]);
  });

  // speakers
  var items = [];
  if(xml.additionalSpeakers.nextSpeaker.length > 0){
    items = xml.additionalSpeakers.nextSpeaker;
  }else{
    items[0] = xml.additionalSpeakers.nextSpeaker;
  }
  $.each(items, function(x, speaker){
    var end = (items.length > x + 1) ? parseInt(items[x+1].slideNumber) : timeline.length;
    for(i=parseInt(speaker.slideNumber); i <= end; i++){
      speakers.push({
        "title": speaker.talkTitle,
        "speaker": speaker.speakerData
      });
    }
  });

  // load video into player
  playerInstance.setup({
    "playlist": [{
      "sources": [{
        "file": xml.level_1.httpVideoLocation
      },{
        "file": xml.level_1.videoLocation
      }]
    }],
    "width": "100%",
    "autostart": true,
    "aspectratio": "16:12"
  }).on('error', function (msg){
    console.log(msg);
  });
  // keep a watch on the video times while playing
  playerInstance.on("time", function(e){
    // e = Object {type: "time", position: 3.1, duration: 1066.733}
    var currentTime = e.position.toString();

    // auto advance slide
    if(timeline.indexOf(currentTime) > 0) changeSlide(timeline.indexOf(currentTime)+1, false)

    // pause video on quiz slides (based on next slides start time)\
    if(pauseTimes.indexOf(currentTime) >= 0) playerInstance.pause(true)
  });

  // load first slide
  changeSlide(1);

  // load resources
  if(xml.relatedResources.resource.length > 0){
    var items = [];
    $.each(xml.relatedResources.resource, function(i, resource){
      items.push($("<li/>",{
        "html": '<a href="'+resource.url+'"><i class="'+chooseIcon(resource.type)+'"></i> '+resource.linkText+'</a>'
      }));
    });
    resources.html(items);
  }

  // load apply for credit if exists
  if(xml.evaluationPath.length > 0) $("<a/>", {
    "href": xml.evaluationPath,
    "text": "Apply For Credit",
    "class": "btn btn-default"
  }).appendTo(info);
}

function changeSlide(index, advanceVideo){
  if (typeof(advanceVideo)==='undefined') advanceVideo = true
  var $this = slides.find("[data-index='" + index + "']");

  // clear presentation
  presentation.removeClass("quiz-active video-active").data("index", index);
  disableScreen.hide();
  // remove video slide if it was present
  if(typeof(videoSlide) != 'undefined') {
    videoSlide.remove();
    presentationSpecial.html("");
    delete videoSlide;
  }

  // change img if not special slide
  if(!$this.data("isvideo") && !$this.data("isquiz")) presentationImg.attr("src", buildSlide(index)).load(function(){});

  // is the slide a video?
  if($this.data("isvideo")){
    presentation.addClass("video-active");
    //presentationSpecial.html(buildVideo($this.data()));
    videoSlide = jwplayer("presentationSpecial");
    videoSlide.setup({
      "playlist": [{
        "sources": [{
          "file": $this.data("videodata").httpVideoLocation
        },{
          "file": $this.data("videodata").videoLocation
        }]
      }],
      "width": "100%",
      "autostart": true,
      "aspectratio": "16:12"
    }).setControls(false);

    // allow play/pause from talking head video to control video slide
    playerInstance.on('pause',function(e){
      videoSlide.pause(true);
    }).on("play",function(e){
      videoSlide.play(true);
    });
  }

  // is the slide a quiz?
  if($this.data("isquiz")){
    disableScreen.fadeIn();
    presentation.addClass("quiz-active");
    presentationSpecial.html(buildQuiz($this.data()));
    // set a timeout on the video based on the next slide's time
    // other option: can come up with the pause times based on quiz slides and add to on("time") event
    // other option: set a javascript timeout here for the difference in seconds between now and next slide.. BAD opiton
    // NEED A PAUSE SLIDE FUNCTION TO NO AUTO ADVANCE SLIDE ON QUIZ AND TO PAUSE VIDEO (RESUME ON ADVANCE)
  }

  // skip video
  if(advanceVideo) playerInstance.seek($this.data("time"));

  // advance slider
  slides.cycle("goto", $this.data("index")-1);

  // change speaker info
  presentationTitle.text(speakers[$this.data("index")].title);
  presentationSpeaker.text(speakers[$this.data("index")].speaker);
}

// helpers
function buildSlide(slide) {
  return slideDir + slidePrefix + pad(slide) + ".jpg?";// + Math.random();
}

function buildQuiz(slide) {
  var questions = [];
  for(i=1; i <= slide.quizdata.anum; i++){
    questions.push($("<label/>",{
      "text": eval("slide.quizdata.a"+i)
    }).prepend($("<input/>",{
      "value": i,
      "checked": i == 1 ? true : false,
      "name": "question",
      "type": "radio"
    })));
  }
  return $("<form/>",{
    "html": '<h3>'+slide.quizdata.question+'</h3>'//<div class="validation alert alert-danger"><strong>Incorrect!</strong> Please try again.</div>'
  }).append(questions).append($("<button/>",{
    "class": "btn btn-primary",
    "text": "Submit"
  }));
}

function chooseIcon(type){
  switch(type){
    case "download":
      return "glyphicon glyphicon-download-alt";
    end;
    case "video":
      return "glyphicon glyphicon-film";
    end;
    case "link":
      return "glyphicon glyphicon-link";
    end;
  }
}

// utilities
function pad(n) {
    return (n < 10) ? ("0" + n) : n;
}